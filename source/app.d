import std.stdio;
import dqml;
import std.path : dirSeparator;
import core.sys.windows.windows;
import std.conv;

void main()
{
    try {
    	/*HANDLE hModule = GetModuleHandle(NULL);
    	HANDLE hResource = LoadResource(hModule, FindResource(hModule, "APP_QML", RT_RCDATA));
    	char *pBytes = cast(char*)LockResource(hResource);*/
    	
        auto app = new QGuiApplication();
        scope(exit) destroy(app);

        auto engine = new QQmlApplicationEngine();
        scope(exit) destroy(engine);

        QResource.registerResource(app.applicationDirPath
            ~ dirSeparator ~ "app.rcc");

        engine.load(new QUrl("qrc:///app.qml"));
        //engine.loadData(to!string(pBytes))

        app.exec();
    }
    catch {
    }
}